(:: pragma bea:global-element-parameter parameter="$obterClienteResponse" element="ns1:ObterClienteResponse" location="../../IdentificarCliente/xsd/Legado/ObterCliente.xsd" ::)
(:: pragma bea:global-element-return element="ns3:outIdentificarCliente" location="../../IdentificarCliente/xsd/IdentificaClienteServiceV2.xsd" ::)

declare namespace ns2 = "http://ws.mediador.lojasrenner.com.br/types/MediatorComplexTypes/V2";
declare namespace ns1 = "http://lojasrenner.com.br/EBS/Renner/ObterCliente/v1";
declare namespace ns3 = "http://ws.mediador.lojasrenner.com.br/types/IdentificaClienteService/V2";
declare namespace ns0 = "http://lojasrenner.com.br/EBO/Renner/v1";
declare namespace xf = "http://tempuri.org/ContaCorrente/transformation/outIdentificarClienteLegado/";

declare function xf:outIdentificarClienteLegado($obterClienteResponse as element(ns1:ObterClienteResponse))
    as element(ns3:outIdentificarCliente) {
        <ns3:outIdentificarCliente>
            <ns3:nome>{ data($obterClienteResponse/cliente/nome) }</ns3:nome>
            <ns3:data-nascimento>{ fn-bea:date-from-dateTime($obterClienteResponse/cliente/data-nascimento) }</ns3:data-nascimento>
            <ns3:cpf>{ xs:string($obterClienteResponse/cliente/cpf) }</ns3:cpf>
            <ns3:telefone>{ xs:integer($obterClienteResponse/cliente/numero-telefone) }</ns3:telefone>
            <ns3:telefone-ddd>{ xs:integer($obterClienteResponse/cliente/numero-ddd) }</ns3:telefone-ddd>
        </ns3:outIdentificarCliente>
};

declare variable $obterClienteResponse as element(ns1:ObterClienteResponse) external;

xf:outIdentificarClienteLegado($obterClienteResponse)
