(:: pragma bea:global-element-parameter parameter="$inIdentificarCliente" element="ns2:inIdentificarCliente" location="../../IdentificarCliente/xsd/IdentificaClienteServiceV2.xsd" ::)
(:: pragma bea:global-element-return element="ns0:identificarClienteCic" location="../../IdentificarCliente/xsd/Totvscard/AutomacaoService.xsd" ::)

declare namespace ns2 = "http://ws.mediador.lojasrenner.com.br/types/IdentificaClienteService/V2";
declare namespace ns1 = "http://ws.mediador.lojasrenner.com.br/types/MediatorComplexTypes/V2";
declare namespace ns0 = "http://ws.automacao.totvscard.totvs.com/v1";
declare namespace xf = "http://tempuri.org/ContaCorrente/transformation/inIdentificarClienteCIC/";

declare function xf:inIdentificarClienteCIC($inIdentificarCliente as element(ns2:inIdentificarCliente))
    as element(ns0:identificarClienteCic) {
        <ns0:identificarClienteCic>
            <inIdentificarClienteCic>
                <numeroCPF>{ fn-bea:pad-left(xs:string($inIdentificarCliente/ns2:cpf), 11, '0') }</numeroCPF>
            </inIdentificarClienteCic>
        </ns0:identificarClienteCic>
};

declare variable $inIdentificarCliente as element(ns2:inIdentificarCliente) external;

xf:inIdentificarClienteCIC($inIdentificarCliente)
