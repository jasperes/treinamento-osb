(:: pragma bea:global-element-parameter parameter="$inIdentificarCliente" element="ns2:inIdentificarCliente" location="../../IdentificarCliente/xsd/IdentificaClienteServiceV2.xsd" ::)
(:: pragma bea:global-element-return element="ns0:inObterProcessadora" location="../../IdentificarCliente/xsd/ProcessadoraServiceV2.xsd" ::)

declare namespace ns2 = "http://ws.mediador.lojasrenner.com.br/types/IdentificaClienteService/V2";
declare namespace ns1 = "http://ws.mediador.lojasrenner.com.br/types/MediatorComplexTypes/V2";
declare namespace ns0 = "http://ws.mediador.lojasrenner.com.br/types/ProcessadoraService/V2";
declare namespace xf = "http://tempuri.org/ContaCorrente/transformation/InObterProcessadora/";

declare function xf:InObterProcessadora($inIdentificarCliente as element(ns2:inIdentificarCliente))
    as element(ns0:inObterProcessadora) {
        <ns0:inObterProcessadora>
            {
                for $cpf in $inIdentificarCliente/ns2:cpf
                return
                    <ns0:cpf>{ data($cpf) }</ns0:cpf>
            }
        </ns0:inObterProcessadora>
};

declare variable $inIdentificarCliente as element(ns2:inIdentificarCliente) external;

xf:InObterProcessadora($inIdentificarCliente)
