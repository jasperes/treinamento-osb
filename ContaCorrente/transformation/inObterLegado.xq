(:: pragma bea:global-element-parameter parameter="$inIdentificarCliente" element="ns2:inIdentificarCliente" location="../../IdentificarCliente/xsd/IdentificaClienteServiceV2.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ObterClienteRequest" location="../../IdentificarCliente/xsd/Legado/ObterCliente.xsd" ::)

declare namespace ns2 = "http://ws.mediador.lojasrenner.com.br/types/IdentificaClienteService/V2";
declare namespace ns1 = "http://ws.mediador.lojasrenner.com.br/types/MediatorComplexTypes/V2";
declare namespace ns0 = "http://lojasrenner.com.br/EBS/Renner/ObterCliente/v1";
declare namespace xf = "http://tempuri.org/ContaCorrente/transformation/inObterLegado/";

declare function xf:inObterLegado($inIdentificarCliente as element(ns2:inIdentificarCliente))
    as element(ns0:ObterClienteRequest) {
        <ns0:ObterClienteRequest>
            <cpf>{ xs:string($inIdentificarCliente/ns2:cpf) }</cpf>
        </ns0:ObterClienteRequest>
};

declare variable $inIdentificarCliente as element(ns2:inIdentificarCliente) external;

xf:inObterLegado($inIdentificarCliente)
