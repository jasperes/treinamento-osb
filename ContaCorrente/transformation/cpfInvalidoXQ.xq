(:: pragma bea:global-element-return element="ns0:ApplicationFault" location="../xsd/contaservice.xsd" ::)

declare namespace ns0 = "http://www.tesouro.gov.br/contacorrente/";
declare namespace xf = "http://tempuri.org/ContaCorrente/transformation/cpfInvalidoXQ/";

declare function xf:cpfInvalidoXQ($errorCode as xs:string,
    $message as xs:string,
    $detail as xs:string)
    as element(ns0:ApplicationFault) {
        <ns0:ApplicationFault>
            <code>{ $errorCode }</code>
            <message>{ $message }</message>
            <details>
                <message>{ $detail }</message>
            </details>
        </ns0:ApplicationFault>
};

declare variable $errorCode as xs:string external;
declare variable $message as xs:string external;
declare variable $detail as xs:string external;

xf:cpfInvalidoXQ($errorCode,
    $message,
    $detail)
