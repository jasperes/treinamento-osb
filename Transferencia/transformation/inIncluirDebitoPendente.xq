(:: pragma bea:global-element-parameter parameter="$efetivarTransferencia" element="ns1:efetivarTransferencia" location="../wsdl/TransferenciaService.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:incluirDebitoPendente" location="../wsdl/ContaCorrenteService.wsdl" ::)

declare namespace ns2 = "http://www.bc.gov.br/transferencia";
declare namespace ns1 = "http://www.bb.com.br/TransferenciaService";
declare namespace ns0 = "http://www.bb.com.br/ContaCorrenteService";
declare namespace xf = "http://tempuri.org/Transferencia/transformation/inIncluirDebitoPendente/";

declare function xf:inIncluirDebitoPendente($efetivarTransferencia as element(ns1:efetivarTransferencia))
    as element(ns0:incluirDebitoPendente) {
        <ns0:incluirDebitoPendente>
            <valor>{ data($efetivarTransferencia/transferencia/ns2:valor) }</valor>
            {
                let $cc-origem := $efetivarTransferencia/transferencia/ns2:cc-origem
                return
                    <conta>
                        <ns2:banco>{ data($cc-origem/ns2:banco) }</ns2:banco>
                        <ns2:agencia>{ data($cc-origem/ns2:agencia) }</ns2:agencia>
                        <ns2:numero>{ data($cc-origem/ns2:numero) }</ns2:numero>
                    </conta>
            }
        </ns0:incluirDebitoPendente>
};

declare variable $efetivarTransferencia as element(ns1:efetivarTransferencia) external;

xf:inIncluirDebitoPendente($efetivarTransferencia)