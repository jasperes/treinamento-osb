(:: pragma bea:global-element-parameter parameter="$consultaContaOut" element="ns0:ConsultaContaOut" location="../xsd/contaservice.xsd" ::)
(:: pragma bea:local-element-complex-return type="ns1:ContaCorrentes/ns1:conta" location="../xsd/proxy.xsd" ::)

declare namespace ns1 = "http://www.cwi.com.br/proxy/";
declare namespace ns0 = "http://www.tesouro.gov.br/contacorrente/";
declare namespace xf = "http://tempuri.org/SplitJoin/transformation/ConsultaConta/";

declare function xf:ConsultaConta($consultaContaOut as element(ns0:ConsultaContaOut))
    as element() {
        <ns1:conta>
            <nome>{ data($consultaContaOut/nome) }</nome>
            <cpf>{ data($consultaContaOut/cpf) }</cpf>
            <conta>{ data($consultaContaOut/conta) }</conta>
            <saldo>{ data($consultaContaOut/saldo) }</saldo>
            <banco>{ data($consultaContaOut/banco) }</banco>
        </ns1:conta>
};

declare variable $consultaContaOut as element(ns0:ConsultaContaOut) external;

xf:ConsultaConta($consultaContaOut)
