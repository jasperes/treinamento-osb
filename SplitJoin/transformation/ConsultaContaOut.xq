xquery version "1.0" encoding "Cp1252";
(:: pragma bea:global-element-return element="ns1:ContasCorrentes" location="../xsd/proxy.xsd" ::)

declare namespace xf = "http://tempuri.org/SplitJoin/transformation/ConsultaContaOut/";
declare namespace ns1 = "http://www.cwi.com.br/proxy/";
declare namespace ns0 = "http://www.tesouro.gov.br/contacorrente/";

declare function xf:ConsultaContaOut()
as element(ns1:ContasCorrentes) {
    <ns1:ContasCorrentes/>
};


xf:ConsultaContaOut()
