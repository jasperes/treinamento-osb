(:: pragma bea:global-element-parameter parameter="$consultaContas" element="ns1:ConsultaContas" location="../xsd/proxy.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ConsultaContaIn" location="../xsd/contaservice.xsd" ::)

declare namespace ns1 = "http://www.cwi.com.br/proxy/";
declare namespace ns0 = "http://www.tesouro.gov.br/contacorrente/";
declare namespace xf = "http://tempuri.org/SplitJoin/transformation/ConsultaContaBBIn/";

declare function xf:ConsultaContaBBIn($consultaContas as element(ns1:ConsultaContas))
    as element(ns0:ConsultaContaIn) {
        <ns0:ConsultaContaIn>
            <numCpf>{ data($consultaContas/numCpf) }</numCpf>
        </ns0:ConsultaContaIn>
};

declare variable $consultaContas as element(ns1:ConsultaContas) external;

xf:ConsultaContaBBIn($consultaContas)
